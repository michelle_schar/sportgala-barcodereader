﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarcodeReader
{
    public class Data
    {
        public List<Sportgala> sportgalaEintraege = new List<Sportgala>();
        String connectionString = "server=localhost,1433;database=MyTest;UID=sa;Password=P@ssword20";

        public List<Sportgala> loadDatabase()
        {
            String query = "SELECT [barcodeId],[Name],[Partei] FROM [dbo].[sportgala]";
            SqlConnection connection = new SqlConnection(connectionString);

            using (var command = new SqlCommand(query, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var db = new Sportgala
                        {
                            barcodeId = reader.GetInt32(0),
                            name = reader.GetString(1),
                            partei = reader.GetString(2)
                        };
                        sportgalaEintraege.Add(db);
                    }
                }
            }
            return sportgalaEintraege;
        }
    }
}

﻿using IronBarCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BarcodeReader
{
    public class Program
    {
        static void Main(string[] args)
        {
            Data data = new Data();
            bool isAuthorized = false;
            string name = "";
            string partei = "";

            do
            {
                Console.WriteLine();
                string barcodeInput = Console.ReadLine();

                foreach (var d in data.loadDatabase())
                {
                    string barcdoeAsString = Convert.ToString(d.barcodeId);
                    if (barcodeInput == barcdoeAsString)
                    {
                        isAuthorized = true;
                        name = d.name;
                        partei = d.partei;
                        break;
                    }
                    else
                    {
                        isAuthorized = false;
                    }
                }

                if (isAuthorized)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine("Authorized");
                    Console.WriteLine("Hello: " + name);
                    Console.WriteLine("Partei: " + partei);
                    Console.BackgroundColor = ConsoleColor.Black;

                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Unauthorized, sorry couldn't find you in the database!");
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.Black;
                }
            } while (true);



            //do
            //{
            //    Console.WriteLine();
            //    barcodeInput = Console.ReadLine();

            //    if (barcodeInput == '')
            //    {
            //        Console.BackgroundColor = ConsoleColor.Green;
            //        Console.WriteLine("Success");
            //        Console.BackgroundColor = ConsoleColor.Black;
            //    }
            //    else
            //    {
            //        Console.BackgroundColor = ConsoleColor.Red;
            //        Console.WriteLine("Failed");
            //        Console.BackgroundColor = ConsoleColor.Black;
            //    }
            //} while (true);
        }
    }
}
